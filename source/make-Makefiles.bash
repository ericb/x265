#!/bin/bash

# Important : if not in the environment,
# the build will fail a strange way

# coupled to ENABLE_SVT_HEVC:BOOL=ON
export SVT_HEVC_INCLUDE_DIR=/usr/include/svt-hevc
export SVT_HEVC_LIBRARY_DIR=/usr/lib/x86_64-linux-gnu
export SVT_HEVC_LIBRARY=/usr/lib/x86_64-linux-gnu/libSvtHevcEnc.so.1

# last git pull. Current version is 1.1.2
export VMAF_LIBRARY=/usr/lib/x86_64-linux-gnu/libvmaf.so.1.1.3

# Run this from within a bash shell
cmake     -G "Unix Makefiles" \
          -Bbuild_linux \
          -D CMAKE_CXX_STANDARD=17 \
          -D CMAKE_CXX_STANDARD_REQUIRED=ON \
          -D CMAKE_BUILD_TYPE=Release \
          -D SVT_VERSION_MINOR_REQUIRED=5 \
          -D NATIVE_BUILD=ON \
          -D ENABLE_CLI=ON \
          -D ENABLE_SHARED=ON \
          -D ENABLE_HDR10_PLUS=ON \
          -D ENABLE_LIBVMAF:BOOL=ON \
          -D VMAF="vmaf" \
          -D ENABLE_SVT_HEVC:BOOL=ON \
          .
##&& ccmake .

#          -D EXTRA_LIB="vmaf" \
#          -D ENABLE_PIC:BOOL=ON \
